# 软工实验复习 UML绘图

> 19231224 卢恒润

> 题型：选择题（或者是选择题的变形）、填空题、画图题
>
> 你应该复习以下资料
>
> - 打印版 PPT
> - 关于视频的笔记
> - 助教提供的资料（例如 git help）
> - 论坛上关于作业的答案说明
> - 实验课的作业
>
> 参考文件：[上机考试说明](file:///C:/Users/lhr4108/Desktop/上机考试说明.pdf)

### UML绘图

#### UML用例图

参考资源：教材p80

> **什么是用例图？**

用例图是用来描述系统功能的技术，表示一个系统中用例与参与者及其关系的图，主要用于需求分析阶段。

用例图的基本组成元素：`参与者`、`用例`、`元素`之间的关系。

用例图使用范围：需求分析

- 捕获需求。描述功能需求、行为需求（系统要完成什么任务）

- 分析需求。明确类和对象，建立之间的关系

![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202556668-1824963836.png)

> 参与者

参与者的概念

- 参与者是与系统主体交互的外部实体的类元，描述了一个或一组与系统产生交互的外部用户或外部事物。
- **参与者位于系统边界之外，而不是系统的一部分**。
- 参与者是从现实世界中抽象出来的一种形式，却不一定确切对应的现实中的某个特定对象。

符号：

![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202615316-466882456.png)

参与者的泛化关系

当系统中的几个参与者既扮演自身的角色，同时也有更一般化的角色时，可以通过建立泛化关系来进行描述。

与类相似，父参与者可以是抽象的，即不能创建一个父参与者的直接实例，这就要求属于抽象父参与者的外部对象一定能够属于其子参与者之一。

![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202630023-2077267415.png)

> 用例

用例的概念

用例是类元提供的一个内聚的的功能单元，表明系统与一个或多个参与者之间信息交换的顺序，也表明了系统执行的动作。

简单来说，用例就是某一个参与者在系统中做某件事从开始到结束的一系列活动的集合，以及结束时应该返回的可观测、有意义的结果，其中也包含可能的各种分支情况。

用例与用例图被广泛使用于系统的需求建模阶段，并在系统的整个生命周期中被不断细化。

符号：

![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202652271-571691186.png)

用例与参与者的关系

一个用例可以隶属一个或多个参与者，一个参与者也可以参与一个或多个用例。用例与参与者之间存在关联关系。

主参与者与次参与者：通常来说主参与者是用例的重要服务对象，而次参与者处于一种协作地位。

![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202703290-1813046165.png)

用例的特征

用例的特征保证用例能够正确地捕捉功能性需求，同时也是判断用例是否准确的依据。

- 用例是动宾短语
- 用例是相对独立的
- 用例是由参与者启动的
- 用例要有可观测的执行结果
- 一个用例是一个单元

用例之间的关系

- 泛化关系
- 依赖关系（包含、扩展）

> 泛化关系

泛化关系：与参与者的泛化关系相似，用例的泛化关系将特化的用例与一般化的用例联系起来。子用例继承了父用例的属性、操作和行为序列，并且可以增加属于自己的附加属性和操作。父用例同样可以定义为抽象用例。

![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202716283-997608144.png)

用例之间的泛化关系表示为一根实线三角箭头，箭头指向父用例一方。

我平时是这样记住这个关系的，就是子类从父类中继承，父类就是子类的泛化。

泛化：子类指向父类

包含：父类指向子类

> 包含(include)

依赖关系——包含

包含指的是一个用例（基用例）可以包含其他用例（包含用例）具有的行为，其中包含用例中定义的行为将被插入基用例定义的行为中。

包含的两个基本约束：

1）基用例可以看到包含用例，并需要依赖于包含用例的执行结果，但是它对包含用例的内部结构没有了解；

2）基用例一定会要求包含用例执行。

包含表示为一个虚线箭头附加上《include》的构造型，箭头从基用例指向包含用例。

 ![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202742870-113020780.png)

> 扩展

扩展指的是一个用例（扩展用例）对另一个用例（基用例）行为的增强。

![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202754538-2017567508.png)

扩展使用一个附加了《enxtend》构造型的虚线箭头表示，箭头指向基用例。

注意：扩展与包含的箭头方向是相反的，这表明扩展取决于扩展用例而非基用例，扩展用例决定扩展的执行时机，基用例对此一无所知。

 ![img](https://img2018.cnblogs.com/blog/1130413/201902/1130413-20190208202807381-623716385.png)

> 包含、扩展的区别

根本区别，包含是无条件执行，扩展是有条件执行。图的起点不同，终点也不同。



在EA建立用例图，参与者

1，新建文件

![image-20210530213255156](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530213255156.png)

2，新建增图

![image-20210530213646879](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530213646879.png)

3，新建图

![image-20210530213428650](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530213428650.png)

4，打开工具箱

alt 5

5，参与者

![image-20210530214125111](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530214125111.png)

6，用例

![image-20210530214708284](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530214708284.png)

7，泛化

![image-20210530215318010](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530215318010.png)

8，包含

![image-20210530220643326](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530220643326.png)

9，导出

![image-20210530221524926](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530221524926.png)

![image-20210530221553219](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210530221553219.png)

格式选BMP

#### 实验题目

> 第一次实验 用例图

> **根据以下描述，完成某培训机构入学管理系统的用例图。**
>
> 某培训机构入学管理系统有报名、交费、就读等多项功能，并有课程表（课程号，课程名，收费标准）、学员登记表（学员号，姓名，电话）、学员选课表（学员号，课程号，班级号）、账目表（学员号，收费金额）等诸多数据表。其各项功能如下：
>
> 1.报名：由报名处负责，需要在学员登记表上进行报名登记，需要查询课程表让学员选报课程，学员所报课程将记录在学员选课表。
>
> 2.交费：由收费处负责，需要根据学员所报课程的收费标准进行收费，然后在账目表上进行记账，并打印收款收据给办理交费的学员。
>
> 3.就读：由培训处负责，其在验证学员收款收据后，根据学员所报课程将学员安排到合适班级就读。

> **根据以下需求的描述，完成** **ASMART** **学习平台的用例图。**
>
> ASMART 学习平台是国家新推出的一个类似于 MOOC 的视频学习平台，致力于打造国家精品课程，每一个有提升愿望的人，都可以在这里学习中国优质的大学课程。
>
> 游客可以在观看视频过程中做题，巩固个人能力，已登录的用户还可以制作错题集，学完还能获得认证证书。
>
> 在个人中心，用户也可以回顾错题集，删去已经掌握的错题。同时，用户也可以上传视频资料和习题。如果课程播放量达到 100 万，也可以申请成为金牌讲师。
>
> 管理员会对平台的资源进行管理，并且审核上传的视频，如果视频违规，将视情况对个人账户进行 1 个月封禁到永久封禁等力度不等的惩罚。
>
> 同时，政府也可以直接提交给管理员视频，由管理员进行录入，还可以从政府管理系统导出该平台的视频播放量。
>
> 该平台也会每日自动导出当日的视频播放量，保存，用于后续数据分析，更好的完善该平台的功能。



> **评分标准**
>
> **•关键参与者必须存在，因为其存在反映了系统的功能。反例：图书管理系统中管理员负责增加图书类目，属于关键参与者，然而在用例图中没有管理员类。缺失一个关键参与者扣0.5分。**
>
> **•用例图需要反映系统有哪些使用者，可以做什么。不需要反映业务逻辑、过程的信息。类似“填写用户信息”不是用例，出现一次扣0.1 分。**
>
> **•用例是一个由系统提供给使用者的功能。类似“登录失败”不是用例，出现一次扣0.1 分。**
>
> **•系统自身不能作为参与者（第三方支付系统是可以作为参与者的，因为不是系统本身），出现一次系统自身作为参与者，扣0.5 分。**
>
> •**extend** **与** **include** **慎用。扣分标准如下：**
>
> •**用例图中到处都是** **extend和include，总共扣0.5分;**
>
> •**extend** **与** **include** **的箭头画错，虚实线搞错，画错一个扣0.1分。**
>
> •**泛化关系慎用。一个关系使用不当扣0.5分。**

参考资料： 

[软工第一次实验作业.pptx](C:\Users\lhr4108\Desktop\学习资料大二下\软件工程\19231224_卢恒润_第一次实验\第一次上机\软工第一次实验作业.pptx)

[第一次实验课件.pdf](C:\Users\lhr4108\Desktop\学习资料大二下\软件工程\19231224_卢恒润_第一次实验\第一次实验课件.pdf) 

<video src="C:\Users\lhr4108\Desktop\学习资料大二下\软件工程\19231224_卢恒润_第一次实验\第一次上机\软工第一次实验视频.mp4"></video>

#### UML类图

> 类图

<img src="https://upload-images.jianshu.io/upload_images/5336514-d05e8edc42eb1469.png?imageMogr2/auto-orient/strip|imageView2/2/format/webp" alt="img" style="zoom: 50%;" />

```shell
-表示private  
#表示protected 
~表示default,也就是包权限  
_下划线表示static  
斜体表示抽象  
```

在UML类图中，常见的有以下几种关系: 

- 泛化（Generalization）
- 实现（Realization）
- 关联（Association)
- 聚合（Aggregation）
- 组合(Composition)
- 依赖(Dependency）

> 泛化

**介绍：**  
 泛化(Generalization)表示类与类之间的继承关系，接口与接口之间的继承关系，或类对接口的实现关系

（1）继承
 **介绍：**   
 继承表示是一个类（称为子类、子接口）继承另外的一个类（称为父类、父接口）的功能，并可以增加它自己的新功能的能力。
 **表示方法：**  
 继承使用**空心三角形+实线**表示。
 **示例：**  
 鸟类继承抽象类动物

![img](https:////upload-images.jianshu.io/upload_images/5336514-dc3ac9dd64968b59.png?imageMogr2/auto-orient/strip|imageView2/2/w/231/format/webp)

（2）实现
 **介绍：**   
 实现表示一个class类实现interface接口（可以是多个）的功能。
 **表示方法：**

1）**矩形表示法**  
 使用**空心三角形+虚线**表示
 比如：大雁需要飞行，就要实现飞()接口

![img](https:////upload-images.jianshu.io/upload_images/5336514-2941b3300988ffe9.png?imageMogr2/auto-orient/strip|imageView2/2/w/314/format/webp)

矩形表示法

2）**棒棒糖表示法**  
 使用**实线**表示

![img](https:////upload-images.jianshu.io/upload_images/5336514-ad59831e8065522a.png?imageMogr2/auto-orient/strip|imageView2/2/w/313/format/webp)

棒棒糖表示法

> 依赖

**介绍：**   
 对于两个相对独立的对象，当一个对象负责构造另一个对象的实例，或者依赖另一个对象的服务时，这两个对象之间主要体现为依赖关系。
 **表示方法：**   
 依赖关系用**虚线箭头**表示。
 **示例：**   
 动物依赖氧气和水。调用新陈代谢方法需要氧气类与水类的实例作为参数

![img](https:////upload-images.jianshu.io/upload_images/5336514-823083d2d77916ae.png?imageMogr2/auto-orient/strip|imageView2/2/w/414/format/webp)

依赖关系

> 关联

**介绍：**   
 对于两个相对独立的对象，当一个对象的实例与另一个对象的一些特定实例存在固定的对应关系时，这两个对象之间为关联关系。
 **表示方法：**   
 关联关系用**实线箭头**表示。
 **示例：**   
 企鹅需要‘知道’气候的变化，需要‘了解’气候规律。当一个类‘知道’另一个类时，可以用关联。

![img](https:////upload-images.jianshu.io/upload_images/5336514-0b5f0d7612a7ca17.png?imageMogr2/auto-orient/strip|imageView2/2/w/271/format/webp)

关联关系

> 聚合

**介绍：**   
 表示一种弱的‘拥有’关系，即has-a的关系，体现的是A对象可以包含B对象，但B对象不是A对象的一部分。 **两个对象具有各自的生命周期**。
 **表示方法：**   
 聚合关系用**空心的菱形+实线箭头**表示。
 **示例：**   
 每一只大雁都属于一个大雁群，一个大雁群可以有多只大雁。当大雁死去后大雁群并不会消失，两个对象生命周期不同。

![img](https:////upload-images.jianshu.io/upload_images/5336514-e63191f4e23f2ad9.png?imageMogr2/auto-orient/strip|imageView2/2/w/297/format/webp)

聚合关系

> 组合

**介绍：**   
 组合是一种强的‘拥有’关系，是一种contains-a的关系，体现了严格的部分和整体关系，**部分和整体的生命周期一样**。
 **表示方法：**  
 组合关系用**实心的菱形+实线箭头**表示，还可以使用连线两端的数字表示某一端有几个实例。
 **示例：**   
 鸟和翅膀就是组合关系，因为它们是部分和整体的关系，并且翅膀和鸟的生命周期是相同的。

![img](https:////upload-images.jianshu.io/upload_images/5336514-dfb604bd1c4408d5.png?imageMogr2/auto-orient/strip|imageView2/2/w/365/format/webp)

组合关系

> 符号说明：

1：一个 

1…1：一个

0..*：零个或多个

*：零个或多个

1..*：一个或多个

0..1：零个或一个

链接：https://www.jianshu.com/p/57620b762160

#### UML顺序图

> **顺序图**

顺序图是交互图的一种形式，它显示对象沿生命线发展，对象之间随时间的交互表示为从源生命线指向目标生命线的消息。顺序图能很好地显示那些对象与其它那些对象通信，什么消息触发了这些通信，顺序图不能很好显示复杂过程的逻辑。

> **生命线**

一条生命线在顺序图中代表一个独立的参与者。表示为包含对象名的矩形，如果它的名字是"self"，则说明该生命线代表控制带顺序图的类元。

![img](https://images2015.cnblogs.com/blog/759818/201602/759818-20160229135447970-1161040158.gif)

> **对象**

系统内部参与用例的一群对象。

三类：边界类、控制类、实体类。依次按照从左至右的顺序排列。

- 控制类：只有一个， 边界类和实体类可以有多个。控制类：控制其他类工作的类。每个用例通常有一个控制类，控制用例中的事件顺序， 控制类也可以在多个用例间共用。

- 边界类：位于系统与外界的交界处，窗体、报表、以及表示通讯协议的类、直接与外 部设备交互的类、直接与外部系统交互的类等都是边界类。
- 实体类：保存要放进持久存储体的信息。持久存储体就是数据库、文件等可以永久存储 数据的介质。实体类可以通过事件流和交互图发现。通常每个实体类在数据库中有相 应的表，实体类中的属性对应数据库表中的字段

> **关系**

同步消息：
    发送者要发送一条消息且接收者已经做好接收这个消息的准备才能传送的消息叫同步消息。实心箭头。消息的名称是被调用者的方法名。<img src="https://img-blog.csdnimg.cn/20190623180241387.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM5NzIyNDc1,size_16,color_FFFFFF,t_70" alt="åæ­¥æ¶æ¯01" style="zoom: 50%;" />

异步消息
    发送者不管接收者是否做好准备都可以发送的消息叫异步消息。
<img src="https://img-blog.csdnimg.cn/20190623181704172.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM5NzIyNDc1,size_16,color_FFFFFF,t_70" alt="异步消息" style="zoom:50%;" />

返回消息
<img src="https://img-blog.csdnimg.cn/20190623182106925.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM5NzIyNDc1,size_16,color_FFFFFF,t_70" alt="返回消息" style="zoom: 50%;" />

自关联消息
<img src="https://img-blog.csdnimg.cn/20190623183528583.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM5NzIyNDc1,size_16,color_FFFFFF,t_70" alt="自关联消息" style="zoom:50%;" />

![image-20210531112744197](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210531112744197.png)

> **组合片段**

组合片段用来解决交互执行的条件和方式，它允许在序列图中 直接表示逻辑组件，用于通过指定条件或子进程的应用区域， 为任何生命线的任何部分定义特殊条件和子进程。

![image-20210531112949024](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210531112949024.png)

#### UML活动图

> **状态**

![image-20210531113716757](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210531113716757.png)

> **转移**

![image-20210531113805449](C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210531113805449.png)

> **分叉与汇合**

分叉用于将动作流分为两个或多个**并发运行的分支**，而**汇合则用于同步这些并发分支**，以达到共同完成一项事务的目的。

对象在运行时可能会存在两个或多个并发运行的控制流，为了对并发的控制流建模，UML中引入了分叉与汇合的概念。

<img src="C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210531114047545.png" alt="image-20210531114047545" style="zoom:50%;" />

> **判断节点**

<img src="C:\Users\lhr4108\AppData\Roaming\Typora\typora-user-images\image-20210531114200371.png" alt="image-20210531114200371"  />

> **泳道**

泳道将活动图中的活动划分为若干组，并把每一组指定给负责这组活动的业务组织，即对象。
在活动图中，泳道区分了负责活动的对象，它明确地表示了哪些活动是由哪些对象进行的。在包含泳道的活动图中，每个活动只能明确地属于一个泳道。

泳道是用垂直实线绘出，垂直线分隔的区域就是泳道。在泳道的上方可以给出泳道的名字或对象的名字，该对象负责泳道内的全部活动。泳道没有顺序，不同泳道中的活动既可以顺序进行也可以并发进行，**动作流和对象流允许穿越分隔线**。

​        <img src="https://images0.cnblogs.com/blog2015/775886/201506/211447504664217.png" alt="img" style="zoom:50%;" />

> **终止节点**

分为**活动终止节点**（activity final nodes）和**流程终止节点**（flow final nodes）

- 活动终止节点表示**整个活动**的结束

  【图形】圆圈+内部实心黑色圆点

​                ![img](https://images0.cnblogs.com/blog2015/775886/201506/211114294045681.png)

- 而流程终止节点表示是**子流程**的结束。

  【图形】圆圈+内部十字叉

​                ![img](https://images0.cnblogs.com/blog2015/775886/201506/211115501078292.png)

原文链接：https://blog.csdn.net/weixin_42119415/article/details/90474097

#### 实验题目

> 任务一：绘制 ASMART 学习平台的类图。
>
> 任务二：绘制“审核视频”用例的顺序图。
>
> 任务三：绘制“审核视频”用例的活动图。





### Git指令的使用

### 数据库设计

### 前端开发知识

### 软件测试



